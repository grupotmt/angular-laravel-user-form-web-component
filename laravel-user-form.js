'use strict';

angular.module('wm.laravel.user-form', [])

.component('laravelUserForm', {

    controller: ['$http', function ($http) {

        var that = this;

        that.save = function () {

            that.save.busy = true;

            $http.post(that.action)
                .then(that.onSaved(),  that.onError())
                ['finally'](function () {
                    delete that.save.busy;
                });
        };

    }],

    bindings: {
        user: '=',
        action: '@',
        formClass: '@',
        onSaved: '&',
        onError: '&'
    },

    templateUrl: 'laravel-user-form.html'
})